# nginx-1.17.1

#### 介绍
nginx 是轻量级的Web服务器。由于配置简单，容量小被应用于各个项目。
在企业级开发中，常常作为前端静态资源的代理的服务器，如果配置的好可支持高并发量。
作用：

反向代理

正向代理

负载均衡

HTTP服务器(包含动静分离)

#### 软件架构
软件架构说明


#### 安装教程

1. 下载
2. 配置conf/nginx.conf
3. 启动

(注：linux上也差不多，这里不多介绍，请自行查找官方文档。)

#### 使用说明

1. 开启Nginx：      start nginx
2. 关闭Nginx：      nginx -s stop 或 nginx -s quit
3. 重新载入Nginx：  nginx -s reload

通常在Windows环境下使用Nginx可能会发生一些小的错误：

1. 创建文件失败
例如：当在nginx.conf文件中设置pid  logs/nginx.pid;启动Nginx，这时候可能会报如下错误：

nginx: [error] CreateFile() "E:\nginx\nginx-1.17.1/logs/nginx.pid" failed

这时候可以通过手动创建文件的方式解决这个问题：
CMD输入：nginx -c conf/nginx.conf运行即可。
解决了所有问题之后，开启Nginx和jetty，在浏览器上输入http://localhost/

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)